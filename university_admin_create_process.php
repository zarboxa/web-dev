<?php

require __DIR__ . '/includes/kernel.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

middleware_admin_university();

// complete validation
validateAndRedirectIfErrors($_POST, [
	'user_name' => 'required|string|min:2|max:100',
	'user_phone' => 'required|string|min:3|max:10',
	'user_job' => 'required|string|min:2|max:255',
	'user_desc' => 'required|string|min:2|max:255',
	'user_note' => 'required|string|min:2|max:255',
	'user_email' => 'required|string|min:10|max:100|email|unique:users,user_email',
	'user_type' => 'required|numeric|exists:user_types,ust_id'
], 'university_admin_create.php');

$password = str_random(12);

$mail = new PHPMailer();

// $mail->SMTPDebug = SMTP::DEBUG_SERVER;
$mail->isSMTP();
$mail->Host       = MAIL_HOST;
$mail->SMTPAuth   = true;
$mail->Username   = MAIL_USERNAME;
$mail->Password   = MAIL_PASSWORD;
$mail->SMTPSecure = MAIL_ENCRYPTION;
$mail->Port       = MAIL_PORT;

$mail->setFrom($authUser['user_email'], $authUser['user_name']);
$mail->addAddress($_POST['user_email'], $_POST['user_name']);
$mail->addReplyTo($authUser['user_email'], $authUser['user_name']);

$mail->isHTML(true);
$mail->Subject = 'You have been added as an admin';

$content = file_get_contents(__DIR__ . '/emails/new_user_added.php');
$content = str_replace(['__name__', '__password__', '__email__'], [$_POST['user_name'], $password, $_POST['user_email']], $content);


$mail->Body    = $content;
$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

$mail->send();

$data = [
	'user_pass' => password_hash($password, PASSWORD_DEFAULT),
	'user_name' => $_POST['user_name'],
	'user_email' => $_POST['user_email'],
	'user_phone' => $_POST['user_phone'],
	'user_job' => $_POST['user_job'],
	'user_type' => $_POST['user_type'],
	'user_desc' => $_POST['user_desc'],
	'user_notes' => $_POST['user_note'],
];

// print_r(array_keys($data));
$keys = array_keys($data);
$values = array_values($data);

$keys = implode(', ', $keys);
$values = implode("', '", $values);

$sql = "INSERT INTO users ({$keys}) VALUES ('{$values}')";
query($sql);

$_SESSION['success'] = 'A new user has been added successfully!';
redirect('university_admin_create.php');

// $sql = "INSERT INTO users (user_name, user_email, user_phone, user_job, user_type, user_desc, user_note) VALUES ('{$_POST['user_name']}', '{$_POST['user_email']}', '{$_POST['user_phone']}', '{$_POST['user_job']}', '{$_POST['user_type']}',  '{$_POST['user_desc']}', '{$_POST['user_note']}')";

// echo $sql;


