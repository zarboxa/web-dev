<?php

include('includes/kernel.php');

middleware_admin_university();

validateAndRedirectIfErrors($_POST, [
	'user_id' => 'required|numeric|exists:users,user_id'
], 'university_admins.php');

if ($authUser['user_id'] == $_POST['user_id']) {
	$_SESSION['error'] = 'You cannot delete your account!';
	redirect('university_admins.php');
}

$sql = "UPDATE users SET user_status = 'CLOSED' WHERE user_id = {$_POST['user_id']}";

query($sql);

$_SESSION['success'] = 'User has been deleted successfully!';
redirect('university_admins.php');
