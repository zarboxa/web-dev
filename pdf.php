<?php

require __DIR__ . '/includes/kernel.php';

use Dompdf\Dompdf;

$dompdf = new Dompdf();

/*
$content = file_get_contents(__DIR__ . '/pdf_content.php');
$name = 'Sarah Tarek';
$date = 'Friday';
$age = 35;
$subjects = get_results("SELECT * FROM subjects");

// include __DIR__ . '/pdf_content.php';
$content = trim($content);
$content = eval("?>$content");
*/

$content = file_get_contents(__DIR__ . '/pdf_content.php');

$data = [
	'__name__' => 'Sarah Tarek',
	'__date__' => 'Friday',
	'__age__' => 35
];

$content = str_replace(array_keys($data), array_values($data), $content);

$dompdf->loadHtml($content);

$dompdf->setPaper('A4', 'portrait');

$dompdf->render();
header('Content-Type: application/pdf');
// $dompdf->stream('certification');
echo $dompdf->output();
