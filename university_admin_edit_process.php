<?php
include('includes/kernel.php');

middleware_admin_university();

validateAndRedirectIfErrors($_GET, [
	'user_id' => 'required|numeric|exists:users,user_id'
], 'university_admin.php');

validateAndRedirectIfErrors($_POST, [
	'user_name' => 'required|string|min:2|max:100',
	'user_phone' => 'required|string|min:3|max:10',
	'user_job' => 'required|string|min:2|max:255',
	'user_desc' => 'required|string|min:2|max:255',
	'user_note' => 'required|string|min:2|max:255',
	'user_email' => 'required|string|min:10|max:100|email',
	'user_type' => 'required|numeric|exists:user_types,ust_id'
], 'university_admin_create.php');


$data = [
	'user_name' => $_POST['user_name'],
	'user_email' => $_POST['user_email'],
	'user_phone' => $_POST['user_phone'],
	'user_job' => $_POST['user_job'],
	'user_type' => $_POST['user_type'],
	'user_desc' => $_POST['user_desc'],
	'user_notes' => $_POST['user_note'],
];

$values = [];
foreach ($data as $key => $value) {
	$values[] = "{$key} = '{$value}'";
}

$values = implode(', ', $values);

$sql = "UPDATE users SET {$values} WHERE user_id = {$_GET['user_id']}";
query($sql);

$_SESSION['success'] = 'User data has been updated successfully!';
redirect("university_admin_edit.php?user_id={$_GET['user_id']}");

