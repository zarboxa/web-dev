<?php
require __DIR__ . '/includes/kernel.php';

middleware_auth();

include('includes/header.php');
include('includes/sidebar.php');

?>

<div class="card-container">
	<div class="row">
		<div class="col-12 col-md-6 m-auto">
			<div class="text-center h3">Update Profile</div>
			<div>
				<b>Email: </b><span><?php echo $authUser['user_email']; ?></span>
			</div>
			<div>
				<b>Job: </b><span><?php echo $authUser['user_job']; ?></span>
			</div>
			<?php echo show_alerts(); ?>
			<form method="POST" class="mt-4" action="profile_update_process.php" enctype="multipart/form-data">
				<div class="form-group">
					<label for="user_name">Name</label>
					<input type="text" name="user_name" placeholder="Name" class="form-control" id="user_name" value="<?php echo $authUser['user_name']; ?>">
				</div>
				<div class="form-group">
					<label for="user_phone">Phone</label>
					<input type="tel" name="user_phone" placeholder="Phone" class="form-control" value="<?php echo $authUser['user_phone']; ?>">
				</div>
				<div class="input-group mb-3">
				  <div class="custom-file">
				    <input type="file" class="custom-file-input" id="inputGroupFile01" name="avatar" aria-describedby="inputGroupFileAddon01">
				    <label class="custom-file-label" for="inputGroupFile01">Choose Profile Picture</label>
				  </div>
				</div>
				<div class="form-group">
					<button class="btn btn-success w-100">Update Profile</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
include('includes/footer.php');
?>