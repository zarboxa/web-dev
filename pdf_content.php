<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		html {
			margin: 0;
		}
		.line-1 {
			padding: 20px 0;
			font-size: 30px;
		}
		.line-2 {
			padding: 20px 0;
			color: #333;
		}
		.title {
			font-size: 45px;
			color: red;
		}
		.certification {
			top: 0;
			left: 0;
			z-index: -1;
			position: absolute;
		}
		.container {
			padding: 60px;
			padding-top: 150px;
		}
		.table {
			width: 100%;
			border-collapse: collapse;
		}
		.table th,
		.table td {
			border: 1px solid #eee;
		}
	</style>
</head>
<body>
	<img class="certification" src="certification.png" alt="certification" width="100%">

	<div class="container">
		<div class="title">
			Hello <b>__name__</b>,
		</div>

		<div class="line-1">
			Hello this is the <b>__date__</b> line if PDF file!!
		</div>

		<div class="line-2">
			This is the second line!! <b>__age__</b>
		</div>

		<!-- <table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Subject</th>
					<th>Grade</th>
					<th>Final Grade</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($subjects as $key => $subject) {
				?>
				<tr>
					<td><?php echo $key + 1; ?></td>
					<td><?php echo $subject['name']; ?></td>
					<td>78</td>
					<td>110</td>
				</tr>
				<?php } ?>
			</tbody>
		</table> -->
	</div>
</body>
</html>