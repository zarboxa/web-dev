<?php
include('includes/kernel.php');

middleware_admin_university();

$page = 1;
$q = '';
$amounts = [1, 10, 25, 50, 100];

if (isset($_GET['q']) && !empty($_GET['q']) && is_string($_GET['q'])) {
  $q = $_GET['q'];
}

$result = get_result("SELECT COUNT(*) FROM users");

$total = $result['COUNT(*)'];
$amount = 10;

if (isset($_GET['amount']) && !empty($_GET['amount']) && is_numeric($_GET['amount']) && in_array($_GET['amount'], $amounts)) {
  $amount = $_GET['amount'];
}

$pageCount = ceil($total / $amount);

if (isset($_GET['page']) && !empty($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] <= $pageCount) {
  $page = $_GET['page'];
}

$offset = ($page - 1) * $amount;

$sql = "SELECT * FROM users";

if (!empty($q)) {
  $sql .= " WHERE user_name LIKE '%{$q}%' OR user_email LIKE '%{$q}'";
}

$sql .= " LIMIT {$amount} OFFSET {$offset}";

$users = get_results($sql);

header('Content-type: application/json');

echo json_encode($users);

