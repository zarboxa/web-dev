
// var isToggeled = false
// function toggleSidebar() {
// 	var sidebar = document.getElementById('sidebar')
// 	var mainContent = document.getElementById('main-content')
// 	if (isToggeled) {
// 		sidebar.style.left = 0
// 		mainContent.style.paddingLeft = '245px'
// 	} else {
// 		sidebar.style.left = '-100%'
// 		mainContent.style.paddingLeft = '15px'
// 	}

// 	isToggeled = !isToggeled
// }
// localStorage.setItem('id', 23423)
// localStorage.getItem('id')
// localStorage.removeItem('id')

function toggleSidebar() {
	let classes = document.body.classList
	if (classes.contains('closed-sidebar')) {
		classes.remove('closed-sidebar')
	} else {
		classes.add('closed-sidebar')		
	}
}
var checkSidebar = function() {
	let classes = document.body.classList
	if (window.innerWidth <= 992) {
		classes.add('closed-sidebar')
	} else {
		classes.remove('closed-sidebar')
	}
}
checkSidebar()

window.addEventListener('resize', checkSidebar)

// setTimeout(function() {
// 	console.log('funstion is called!')
// }, 3000)

// setInterval(function() {
// 	console.log('interval is called!!')
// }, 1000)

// var ctx = document.getElementById('myChart').getContext('2d');
// var myChart = new Chart(ctx, {
//     type: 'bar',
//     data: {
//         labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
//         datasets: [{
//             label: '# of Votes',
//             data: [12, 19, 3, 5, 2, 3],
//             backgroundColor: [
//                 'rgba(255, 99, 132, 0.2)',
//                 'rgba(54, 162, 235, 0.2)',
//                 'rgba(255, 206, 86, 0.2)',
//                 'rgba(75, 192, 192, 0.2)',
//                 'rgba(153, 102, 255, 0.2)',
//                 'rgba(255, 159, 64, 0.2)'
//             ],
//             borderColor: [
//                 'rgba(255, 99, 132, 1)',
//                 'rgba(54, 162, 235, 1)',
//                 'rgba(255, 206, 86, 1)',
//                 'rgba(75, 192, 192, 1)',
//                 'rgba(153, 102, 255, 1)',
//                 'rgba(255, 159, 64, 1)'
//             ],
//             borderWidth: 1
//         }]
//     },
//     options: {
//         scales: {
//             yAxes: [{
//                 ticks: {
//                     beginAtZero: true
//                 }
//             }]
//         }
//     }
// });

