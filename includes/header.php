<!DOCTYPE html>
<html dir="rtl">
<head>
	<title>Dashboard</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css?kj">
	<link rel="stylesheet" type="text/css" href="css/dashboard.css?md">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</head>
<body>
	<nav class="navbar fixed-top navbar-expand-lg navbar-light">
		<button class="navbar-toggler toggle-sidebar" type="button" onclick="toggleSidebar()">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <a class="navbar-brand" href="#">Navbar</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNavDropdown">
	    <ul class="navbar-nav">
	      <li class="nav-item active">
	        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#">Features</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#">Pricing</a>
	      </li>
	      <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	        	<img src="<?php echo !empty($authUser['user_avatar']) ? $authUser['user_avatar'] : 'images/default-avatar.png'; ?>" width="20px" height="20px" class="rounded-circle">
	          <?php echo $authUser['user_name']; ?>
	        </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
	          <a class="dropdown-item" href="profile.php">
	          	<span class="fa fa-user"></span>
	          	<span>Profile</span>
	          </a>
	          <a class="dropdown-item" href="change_password.php">
	          	<span class="fa fa-lock"></span>
	          	<span>Change password</span>
	          </a>
	          <a class="dropdown-item" href="logout.php">
	          	<span class="fa fa-power-off"></span>
	          	<span>Logout</span>
	          </a>
	        </div>
	      </li>
	    </ul>
	  </div>
	</nav>


	<div class="main-content" id="main-content">

