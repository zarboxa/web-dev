<?php

session_start();

require __DIR__ . '/../vendor/autoload.php';

// constants
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'secret');
define('DB_DATABASE', 'ac_sys');

// Mail constants
define('MAIL_HOST', 'smtp.gmail.com');
define('MAIL_PORT', 587);
define('MAIL_USERNAME', 'webdev232@gmail.com');
define('MAIL_PASSWORD', 'Dfgmd456JghfkftgR');
define('MAIL_ENCRYPTION', 'TLS');


function show_alerts() {
	$errors = [];
	if (isset($_SESSION['errors']) && !empty($_SESSION['errors']) && is_array($_SESSION['errors'])) {
		$errors = $_SESSION['errors'];
		unset($_SESSION['errors']);
	}

	$txt = '';
	if (count($errors) > 0) {
		$txt .= '<ul class="alert alert-danger">';

		foreach ($errors as $error) {
			$txt .= "<li class='ml-3'>" . $error . "</li>";
		}
			
		$txt .= '</ul>';
	}

	if (isset($_SESSION['success']) && is_string($_SESSION['success'])) {
		$txt .= "<div class='alert alert-success'>{$_SESSION['success']}</div>";
		unset($_SESSION['success']);
	}

	if (isset($_SESSION['error']) && is_string($_SESSION['error'])) {
		$txt .= "<div class='alert alert-danger'>{$_SESSION['error']}</div>";
		unset($_SESSION['error']);
	}

	return $txt;
}

function old_data($key) {
	if (isset($_SESSION['data']) && is_array($_SESSION['data']) && isset($_SESSION['data'][$key])) {
		$value = $_SESSION['data'][$key];
		unset($_SESSION['data'][$key]);
		return $value;
	}
}

function redirect($path) {
	header("Location: $path");
	exit;
}

// fetch results / result
// query
$connection = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

function query($sql) {
	global $connection;

	$query = mysqli_query($connection, $sql);

	if (mysqli_error($connection)) {
		echo mysqli_error($connection);
		die;
	}

	return $query;
}

function get_results($sql) {
	$query = query($sql);

	$data = [];
	while ($row = mysqli_fetch_assoc($query)) {
		$data[] = $row;
	}

	return $data;
}

function get_result($sql) {
	$query = query($sql);

	return mysqli_fetch_assoc($query);
}

function validator($data, $rules){
	$errors = [];

	foreach ($rules as $k => $v) {

		$v = explode('|', $v);

		if (in_array('required', $v) && !isset($data[$k]) ){
			$errors[] = $k.' field is required.';
		}

		if (in_array('required', $v) && empty($data[$k]) ){
			$errors[] = $k.' field has not a value.';
		}

		if ( !empty($data[$k]) ) {
			foreach ($v as $x => $y) {

				if ($y == 'string' && !is_string($data[$k]) ){
					$errors[] = $k.' field must be a string.';
				}
				
				if ($y == 'number' && !is_numeric($data[$k]) ){
					$errors[] = $k.' field must be a number.';
				}

				if ($y == 'array' && !is_array($data[$k]) ){
					$errors[] = $k.' field must be an array.';
				}

				if ($y == 'email' && !filter_var($data[$k], FILTER_VALIDATE_EMAIL) ){
					$errors[] = $k.' field must be a valid email.';
				}

				if ($y == 'confirmed' && !isset($data[ "{$k}_confirmation" ]) ){
					$errors[] = $k.' field must be confirmed.';
				}

				if ($y == 'confirmed' && !empty($data[ "{$k}_confirmation" ]) && $data[$k] != $data["{$k}_confirmation"]){
					$errors[]=$k." field confirmation doesn't match.";
				}

				if (substr($y,0,3) == 'max'){

					$length = substr($y,4);

					if( strlen($data[$k]) > $length){
						$errors[] = $k.' field must not bigger than '.$length;
					}
				}

				if (substr($y,0,3) == 'min'){

					$length = substr($y,4);

					if( strlen($data[$k]) < $length){
						$errors[] = $k.' field must not less than '.$length;
					}
				}

				/*
				if (substr($y,0,2) == 'in'){

					$arr = explode(',', substr($y,3));
					if (!in_array($data[$k], $arr)) {
						# code...
					}
				}
				*/

				if (substr($y ,0 ,6) == 'unique'){
					$t = explode( ',', substr($y, 7) );
	
					if(empty($t[0]) || empty($t[1])){
						throw new Exception("Your Unique parameters is not valid");
					}
					$exist = get_result("SELECT * FROM {$t[0]} WHERE $t[1]='{$data[$k]}' LIMIT 1");

					if ($exist) {
						$errors[] = $data[$k].' has been taken.';
					}					
				}

				if (substr($y, 0, 6) == 'exists'){
					$t = explode( ',', substr($y, 7) );
	
					if(empty($t[0]) || empty($t[1])){
						throw new Exception("Your Exists parameters is not valid");
					}
					$exist = get_result("SELECT * FROM {$t[0]} WHERE $t[1]='{$data[$k]}' LIMIT 1");

					if (!$exist) {
						$errors[] = $data[$k]." doesn't match our records.";
					}					
				}
			}
		}
	}
	return $errors;
}

function validateAndRedirectIfErrors($data, $rules, $path) {
	$errors = validator($data, $rules);

	if (count($errors) > 0) {
		$_SESSION['errors'] = $errors;
		$_SESSION['data'] = $data;

		redirect($path);
	}
}

function str_random($length = 10) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}

$authUser = null;

if (isset($_SESSION['user_id']) && is_numeric($_SESSION['user_id'])) {
	$authUser = get_result("SELECT * FROM users WHERE user_id = {$_SESSION['user_id']}");
}

function middleware_admin_university() {
	global $authUser;

	if (empty($authUser) || (!empty($authUser) && $authUser['user_type'] != 1)) {
		redirect('login.php');
	}
}

function middleware_auth() {
	global $authUser;

	if (empty($authUser)) {
		redirect('login.php');
	}
}

function middleware_guest() {
	global $authUser;

	if (!empty($authUser)) {
		redirect('dashboard.php');
	}
}







