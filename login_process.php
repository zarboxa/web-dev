<?php

require 'includes/kernel.php';

middleware_guest();

validateAndRedirectIfErrors($_POST, [
	'email' => 'required|string|min:10|max:100|email|exists:users,user_email',
	'password' => 'required|string|max:255'
], 'login.php');

$user = get_result("SELECT * FROM users WHERE user_email = '{$_POST['email']}' LiMIT 1");

if ($user['user_status'] === 'CLOSED') {
	$_SESSION['error'] = 'Your account is closed, please contact our support!';
	redirect('login.php');
} elseif (password_verify($_POST['password'], $user['user_pass'])) {
	$_SESSION['user_id'] = $user['user_id'];
	redirect('dashboard.php');
} else {
	$_SESSION['error'] = 'Password is wrong!';
	redirect('login.php');
}
