<?php

require 'includes/kernel.php';

session_destroy();

redirect('login.php');
