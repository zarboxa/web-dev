<?php
include('includes/kernel.php');

middleware_auth();

include('includes/header.php');
include('includes/sidebar.php');

$userTypes = get_results("SELECT * FROM user_types");

?>


<div class="card-container">
	<div class="row">
		<div class="col-12 col-md-6 m-auto">
			<div class="text-center h3">Change password</div>
			<?php echo show_alerts(); ?>
			<form method="POST" action="change_password_process.php">
				<div class="form-group">
					<label for="old_password">Old Password</label>
					<input type="password" name="old_password" placeholder="Password" class="form-control" id="old_password">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" placeholder="Password" class="form-control">
				</div>
				<div class="form-group mb-4">
					<label for="password_confirmation">Password confirmation</label>
					<input type="password" name="password_confirmation" placeholder="Password confirmation" class="form-control">
				</div>
				<div class="form-group">
					<button class="btn btn-primary w-100">Change password</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
include('includes/footer.php');
?>