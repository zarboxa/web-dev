<?php
$servername = "localhost";
$username = "root";
$password = "";

try {
    $conn = new PDO("mysql:host=$servername;dbname=ac_sys", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully";
    $sql="SELECT stinfo_id c1, concat(stinfo_name1 ,' ', stinfo_name2 ,' ', stinfo_name3 ,' ', stinfo_name4) c2 FROM students_info ";
    $stmt=$conn->prepare($sql);
    $stmt->execute();
    $result=$stmt->fetchAll();

    $sql1="SELECT br_id,br_name FROM branches";
    $stm1=$conn->prepare($sql1);
    $stm1->execute();
    $res=$stm1->fetchAll();

    $sql2="SELECT ch_id,ch_name FROM channels";
    $stm2=$conn->prepare($sql2);
    $stm2->execute();
    $res2=$stm2->fetchAll();
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }
?>

<!DOCTYPE html>
<html lang="ar" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Students</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
      /* body{
          direction: rtl;
      } */   
      .col-sm-3.mb-3 {
          padding-top: 5px;
          padding-bottom: 10px;
      }
      button{
        margin: 10px;
      }
       select{ padding:10px;
          width: auto;
          margin:10px;
      } 
      form{height: auto;
          /* direction: rtl; */
          padding:10px;
          margin:20px;
          background-color: rgb(252, 240, 240);
      }
  </style>
</head>
<body>
    
    <form class="needs-validation row" novalidate>
        <div class="form-row">
          <div class="col-sm-4 mb-3">
            <label for="validationTooltip01"   >الرقم التسلسلي</label>
            <input type="text" class="form-control" id="validationTooltip01" value="Mark" disabled  >
            <!-- <div class="valid-tooltip">
              Looks good!
            </div> -->
          </div>
          <div class="col-sm-4 mb-3">
            <label >اسم الطالب </label>    
            <!-- for="validationTooltip02" -->
            <div class="input-group">
            <!--<input type="text" class="form-control"  required>-->
            <select  >

            <?php 
            foreach($result as $i) {?>
            <option value="<?php  echo ($i["c1"]) ; ?>"> 
                    <?php  echo ($i["c2"]) ; ?>
             </option>
            
            <?php } ?>
            </select>
            </div>
          </div>
          <div class="col-sm-4 mb-3">
            <label for="validationTooltip02">اسم المدرسة</label>
            <div class="input-group">
            <input type="text" class="form-control" id="validationTooltip03"  required>
            
          </div></div>
          <div class="col-sm-4 mb-3">
            <label for="validationTooltip02">الرقم الامتحاني </label>
            <div class="input-group">
            <input type="number" class="form-control" id="validationTooltip04"  required>
            
          </div>
          </div>
          <hr>
          <div class="col-sm-4 mb-3">
            <label for="validationTooltip02">اسم الفرع</label>
            <div class="input-group">
            <select  >

            <?php 
            foreach($res as $x) {?>
            <option value="<?php  echo ($x["br_id"]) ; ?>"> 
                    <?php  echo ($x["br_name"]) ; ?>
             </option>
            
            <?php } ?>
            </select>
            <!-- <input type="text" class="form-control" id="validationTooltip05"  required> -->
            
          </div></div>
          <div class="col-sm-4 mb-3">
            <label for="validationTooltipUsername">قناة القبول</label>
            <div class="input-group">
              <!-- <div class="input-group-prepend">
                <span class="input-group-text" id="validationTooltipUsernamePrepend">@</span>
                
              </div> -->
              <select  >

            <?php 
            foreach($res2 as $y) {?>
            <option value="<?php  echo ($y["ch_id"]) ; ?>"> 
                    <?php  echo ($y["ch_name"]) ; ?>
             </option>
            
            <?php } ?>
            </select>
              <!-- <input type="text" class="form-control" id="validationTooltipUsername" aria-describedby="validationTooltipUsernamePrepend" required> -->
              
            </div>
          </div>
          <div class="col-sm-4 mb-3 " style="float: none;">
            <label for="validationTooltip04">الدور الذي نجح فيه</label>
             <select class="custom-select form-control" id="validationTooltip04"  required>
               <option selected value="ذكر">اختر</option>
               <option>الاول</option>
               <option>الثاني</option>
               <option>التكميلي</option>
             </select>
            
           </div>
          <div class="col-sm-4 mb-3">
            <label for="validationTooltipUsername">مجموع الدرجات بدون اضافة</label>
            <div class="input-group">
              <!-- <div class="input-group-prepend">
                <span class="input-group-text" id="validationTooltipUsernamePrepend">@</span>
              </div> -->
              <input type="number" class="form-control" id="validationTooltipUsername" aria-describedby="validationTooltipUsernamePrepend" required>
              
            </div>
          </div>
          
          
          <div class="col-sm-4 mb-3">
            <label for="validationTooltipUsername">معدل الطالب بدون اضافة</label>
            <div class="input-group">
              <!-- <div class="input-group-prepend">
                <span class="input-group-text" id="validationTooltipUsernamePrepend">@</span>
              </div> -->
              <input type="number" class="form-control" id="validationTooltipUsername" aria-describedby="validationTooltipUsernamePrepend" required>
              
            </div>
          </div>
          
          <div class="col-sm-4 mb-3 " style="width: 100%;">
            <label for="validationTooltipUsername">الملاحظات</label>
            <div class="input-group" style="width: 80%;">
              <!-- <div class="input-group-prepend">
                <span class="input-group-text" id="validationTooltipUsernamePrepend">@</span>
              </div> -->
              <textarea class="form-control" id="validationTooltipUsername" aria-describedby="validationTooltipUsernamePrepend" required></textarea>
              
            </div>
          </div>
        
        
             
          <div class="col-sm-4 mb-3">
              <button class="btn btn-primary" type="submit">ارسال</button>
              <button class="btn btn-primary" type="reset">الغاء</button>
      </form>

</body>
</html>