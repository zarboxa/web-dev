<!DOCTYPE html>
<html>
<head>
	<title>New user added</title>
</head>
<body>
	<div>
		Hello __name__,
		<div>
			You have been added as an admin to our dashboard with:
			<div>
				email: __email__
			</div>
			<div>
				password: __password__
			</div>
		</div>
		<div>
			you can login in <a href="/">website</a>
		</div>
	</div>

</body>
</html>