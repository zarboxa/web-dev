<?php

require __DIR__ . '/includes/kernel.php';

middleware_auth();

validateAndRedirectIfErrors($_POST, [
	'user_name' => 'required|string|min:2|max:100',
	'user_phone' => 'required|string|min:3|max:10',
], 'profile.php');

$path = $authUser['user_avatar'];
if (isset($_FILES['avatar']) && !empty($_FILES['avatar'])) {

	$tmp_name = $_FILES['avatar']['tmp_name'];
	$name = $_FILES['avatar']['name'];

	$name = explode('.', $name);
	$extension = array_pop($name);

	// if (!in_array($extension, ['jpe', 'gif'])) {
	// 	$_SESSION['error'] = 'image extension is not valid';
	// 	redirect('profile.php');
	// }

	// if ($_FILES['avatar']['size'] > 5000) {
	// 	$_SESSION['error'] = 'image should be less than 5mb';
	// }

	$name = str_random(50);

	$path = 'uploads/' . $name . '.' . $extension;
	move_uploaded_file($tmp_name, __DIR__ . '/' . $path);

	// uploads/name.png
	$oldAvatar = __DIR__ . '/' . $authUser['user_avatar'];
	if (!empty($authUser['user_avatar']) && file_exists($oldAvatar)) {
		unlink($oldAvatar);
	}
}

$sql = "UPDATE users SET user_name = '{$_POST['user_name']}', user_phone = '{$_POST['user_phone']}', user_avatar = '{$path}' WHERE user_id = {$authUser['user_id']}";

query($sql);

$_SESSION['success'] = 'Profile has been updated successfully!';
redirect('profile.php');
