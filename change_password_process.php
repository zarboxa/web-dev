<?php

require 'includes/kernel.php';

middleware_auth();

validateAndRedirectIfErrors($_POST, [
	'old_password' => 'required|string|max:200',
	'password' => 'required|string|max:255|confirmed'
], 'change_password.php');


if (!password_verify($_POST['old_password'], $authUser['user_pass'])) {
	$_SESSION['error'] = 'Your old password is not correct!';
	redirect('change_password.php');
}

$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
$sql = "UPDATE users SET user_pass = '{$password}' WHERE user_id = {$authUser['user_id']}";

query($sql);

$_SESSION['success'] = 'Password has been chnaged successfully!';
redirect('change_password.php');