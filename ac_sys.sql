-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 15, 2020 at 03:37 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ac_sys`
--

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
CREATE TABLE IF NOT EXISTS `branches` (
  `br_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'الرقم التسلسلي',
  `br_name` varchar(255) NOT NULL COMMENT 'اسم الفرع',
  PRIMARY KEY (`br_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='جدول فروع او تخصص الثانوي ادبي- علمي - الخ';

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`br_id`, `br_name`) VALUES
(1, 'scientific'),
(2, 'letral\r\n'),
(3, 'احيائي'),
(4, 'تطبيقي'),
(7, 'دراسات'),
(8, 'معهد'),
(9, 'تجارة'),
(10, 'صناعة');

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

DROP TABLE IF EXISTS `channels`;
CREATE TABLE IF NOT EXISTS `channels` (
  `ch_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `ch_name` varchar(255) NOT NULL COMMENT 'اسم قناة القبول',
  PRIMARY KEY (`ch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='جدول قنوات القبول';

--
-- Dumping data for table `channels`
--

INSERT INTO `channels` (`ch_id`, `ch_name`) VALUES
(1, 'مركزي'),
(2, 'موازي'),
(3, 'ذوي شهداء'),
(4, 'ابناء تدريسيين'),
(5, 'تعديل ترشيح');

-- --------------------------------------------------------

--
-- Table structure for table `college`
--

DROP TABLE IF EXISTS `college`;
CREATE TABLE IF NOT EXISTS `college` (
  `col_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `col_name` varchar(100) NOT NULL COMMENT 'اسم الكلية',
  `col_max_std` int(11) NOT NULL COMMENT 'الحد الاعلى للطلبة',
  `col_desc` varchar(255) NOT NULL COMMENT 'وصف عام',
  `col_notes` text NOT NULL COMMENT 'ملاحظات',
  PRIMARY KEY (`col_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `college`
--

INSERT INTO `college` (`col_id`, `col_name`, `col_max_std`, `col_desc`, `col_notes`) VALUES
(1, 'كلية الطب', 200, '', 'لا توجد');

-- --------------------------------------------------------

--
-- Table structure for table `deparments`
--

DROP TABLE IF EXISTS `deparments`;
CREATE TABLE IF NOT EXISTS `deparments` (
  `dep_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `dep_name` varchar(50) NOT NULL COMMENT 'اسم القسم',
  `dep_max_std` int(11) NOT NULL COMMENT 'الحد الاعلى من الطلاب',
  `dep_minsum` int(11) NOT NULL COMMENT 'الحد الادى من المجموع',
  `dep_minavg` double NOT NULL COMMENT 'الحد الادنى من المعدلات',
  `dep_desc` varchar(250) NOT NULL COMMENT 'وصف عام',
  `dep_notes` text NOT NULL COMMENT 'ملاحظات',
  `dep_colid` int(11) NOT NULL COMMENT 'الكلية',
  PRIMARY KEY (`dep_id`),
  KEY `fk10` (`dep_colid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول الاقسام';

-- --------------------------------------------------------

--
-- Table structure for table `dep_students`
--

DROP TABLE IF EXISTS `dep_students`;
CREATE TABLE IF NOT EXISTS `dep_students` (
  `dstd_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `dstd_stdid` int(11) NOT NULL COMMENT 'الطالب',
  `dstd_depid` int(11) NOT NULL COMMENT 'الكلية والقسم',
  `dstd_yearid` int(11) NOT NULL COMMENT 'السنة الدراسية',
  `dstd_desc` varchar(255) NOT NULL COMMENT 'وصف عام',
  `dstd_notes` text NOT NULL COMMENT 'ملاحظات',
  PRIMARY KEY (`dstd_id`),
  KEY `fk11` (`dstd_stdid`),
  KEY `fk20` (`dstd_depid`),
  KEY `fk21` (`dstd_yearid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول لربط الطلاب بالاقسام';

-- --------------------------------------------------------

--
-- Table structure for table `exceptions`
--

DROP TABLE IF EXISTS `exceptions`;
CREATE TABLE IF NOT EXISTS `exceptions` (
  `exc_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `exc_name` varchar(255) NOT NULL,
  `exc_chid` int(11) NOT NULL,
  `exc_numbr` int(11) NOT NULL,
  `exc_avg` float NOT NULL,
  `exc_rate` float NOT NULL,
  `exc_desc` varchar(255) NOT NULL,
  `exc_notes` text NOT NULL,
  `exc_rulid` int(11) NOT NULL,
  PRIMARY KEY (`exc_id`),
  KEY `fk29` (`exc_chid`),
  KEY `fk30` (`exc_rulid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `favarate_objects`
--

DROP TABLE IF EXISTS `favarate_objects`;
CREATE TABLE IF NOT EXISTS `favarate_objects` (
  `fav_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `fav_name` varchar(255) NOT NULL COMMENT 'اسم المادة',
  `fav_mindeg` float NOT NULL COMMENT 'الحد الادنى من درجة المادة ',
  `fav_ruleid` int(11) NOT NULL COMMENT 'رقم قانون التقديم من جدول القوانين',
  `fav_desc` varchar(255) NOT NULL COMMENT 'وصف عام',
  `fav_notes` text NOT NULL COMMENT 'ملاحظات',
  PRIMARY KEY (`fav_id`),
  KEY `fk27` (`fav_ruleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول دروس المفاضلة';

-- --------------------------------------------------------

--
-- Table structure for table `interfaces`
--

DROP TABLE IF EXISTS `interfaces`;
CREATE TABLE IF NOT EXISTS `interfaces` (
  `infc_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `infc_name` varchar(255) NOT NULL COMMENT 'اسم الواجهة',
  `infc_desc` varchar(255) NOT NULL COMMENT 'وصف عام',
  `infc_notes` text NOT NULL COMMENT 'ملاحظات',
  PRIMARY KEY (`infc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول الواجهات';

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `perm_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `perm_usrid` int(11) NOT NULL,
  `perm_interface` int(11) NOT NULL,
  `perm_full` tinyint(1) NOT NULL,
  `perm_exec` tinyint(1) NOT NULL,
  `perm__insert` tinyint(1) NOT NULL,
  `perm_update` tinyint(1) NOT NULL,
  `perm_delete` tinyint(1) NOT NULL,
  `perm_grantto` tinyint(1) NOT NULL,
  `perm_desc` varchar(255) NOT NULL,
  `perm_notes` text NOT NULL,
  PRIMARY KEY (`perm_id`),
  KEY `fk40` (`perm_usrid`),
  KEY `fk41` (`perm_interface`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `rules`
--

DROP TABLE IF EXISTS `rules`;
CREATE TABLE IF NOT EXISTS `rules` (
  `rul_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `rul_depid` int(11) NOT NULL COMMENT 'القسم والكلية والسنة الدراسية',
  `rul_sex` varchar(10) NOT NULL COMMENT 'ذكر او انثى او اخرى',
  `rul_branchid` int(11) NOT NULL COMMENT 'رقم الفرع في الثانوية',
  `rul_rate` double NOT NULL COMMENT 'نسبة حسب التحديدات',
  `rul_number` int(11) NOT NULL COMMENT 'العدد حسب التحديدات',
  `rul_avgfrom` float NOT NULL COMMENT 'المعدل من',
  `rul_avgto` float NOT NULL COMMENT 'المعدل الى ',
  `rul_sumfrom` int(11) NOT NULL COMMENT 'المجموع من',
  `rul_sumto` int(11) NOT NULL COMMENT 'المجموع الى',
  `rul_channelid` int(11) NOT NULL COMMENT 'رقم قناة القبول',
  `rul_desc` varchar(255) NOT NULL COMMENT 'وصف عام',
  `rul_notes` text NOT NULL COMMENT 'ملاحظات',
  `rul_yrid` int(11) NOT NULL,
  PRIMARY KEY (`rul_id`),
  KEY `fk24` (`rul_depid`),
  KEY `fk25` (`rul_branchid`),
  KEY `fk26` (`rul_channelid`),
  KEY `fk99` (`rul_yrid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول قوانين التقديم على الاقسام';

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `set_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `set_adddeg_attempt1` float NOT NULL COMMENT 'اضافة درجة للدور الاول',
  `set_chid` int(11) NOT NULL COMMENT ' قناة القبول',
  `set_chid_degree` float NOT NULL COMMENT 'اضافة درجة حسب قناة القبول',
  `set_end_date` date NOT NULL COMMENT 'تاريخ نهاية التقديم',
  `set_desc` varchar(255) NOT NULL,
  `set_notes` text NOT NULL,
  `set_yrid` int(11) NOT NULL,
  PRIMARY KEY (`set_id`),
  KEY `fk31` (`set_chid`),
  KEY `fk33` (`set_yrid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول الاعدادات العامة';

-- --------------------------------------------------------

--
-- Table structure for table `std_files`
--

DROP TABLE IF EXISTS `std_files`;
CREATE TABLE IF NOT EXISTS `std_files` (
  `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `f_filename` varchar(100) NOT NULL COMMENT 'اسم الملف',
  `f_type` varchar(50) NOT NULL COMMENT 'نوع الملف',
  `f_size` int(11) NOT NULL COMMENT 'حجم الملف',
  `f_content` mediumblob NOT NULL COMMENT 'محتوى',
  `f_stdid` int(11) NOT NULL COMMENT 'رقم الطالب',
  `f_notes` text NOT NULL COMMENT 'ملاحظات',
  `f_desc` varchar(100) NOT NULL COMMENT 'اسم المستمسك',
  PRIMARY KEY (`f_id`),
  KEY `fk7` (`f_stdid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `students_info`
--

DROP TABLE IF EXISTS `students_info`;
CREATE TABLE IF NOT EXISTS `students_info` (
  `stinfo_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'الرقم التسلسلي',
  `stinfo_name1` varchar(25) NOT NULL COMMENT 'الاسم الاول',
  `stinfo_name2` varchar(25) NOT NULL COMMENT 'اسم الاب',
  `stinfo_name3` varchar(25) NOT NULL COMMENT 'اسم الجد',
  `stinfo_name4` varchar(25) NOT NULL COMMENT 'اسم الجد الرابع',
  `stinfo_surname` varchar(25) NOT NULL COMMENT 'اللقب',
  `stinfo_mothername` varchar(100) NOT NULL COMMENT 'اسم الام الثلاثي',
  `stinfo_sex` varchar(50) NOT NULL DEFAULT 'ذكر' COMMENT 'الجنس',
  `stinfo_address` varchar(250) NOT NULL COMMENT 'العنوان',
  `stinfo_email` varchar(150) NOT NULL COMMENT 'البريد الالكتروني',
  `stinfo_phone` varchar(150) NOT NULL COMMENT 'رقم الموبايل',
  `stinfo_notes` longtext NOT NULL COMMENT 'الملاحظات',
  `stinfo_idcard` varchar(250) NOT NULL COMMENT 'رقم البطاقة',
  `stinfo_idno` varchar(250) NOT NULL COMMENT 'رقم الجنسية',
  `stinfo_certid` varchar(250) NOT NULL COMMENT 'رقم شهادة الجنسية',
  `stinfo_foodcardno` varchar(250) NOT NULL COMMENT 'ؤقم بطاقة العذائية',
  `stinfo_addresscard` varchar(250) NOT NULL COMMENT 'بطاقة السكن',
  `stinfo_kafeelname` varchar(255) NOT NULL COMMENT 'اسم الكفيل',
  `stinfo_kafeeladdress` varchar(255) NOT NULL COMMENT 'عنوان الكفيل',
  `stinfo_kafeelidno` varchar(255) NOT NULL COMMENT 'رقم هاتف الكفيل',
  `stinfo_kafeeljob` varchar(255) NOT NULL COMMENT 'جهة انتساب الكفيل',
  PRIMARY KEY (`stinfo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='جدول معلومات الطلاب عامة ';

--
-- Dumping data for table `students_info`
--

INSERT INTO `students_info` (`stinfo_id`, `stinfo_name1`, `stinfo_name2`, `stinfo_name3`, `stinfo_name4`, `stinfo_surname`, `stinfo_mothername`, `stinfo_sex`, `stinfo_address`, `stinfo_email`, `stinfo_phone`, `stinfo_notes`, `stinfo_idcard`, `stinfo_idno`, `stinfo_certid`, `stinfo_foodcardno`, `stinfo_addresscard`, `stinfo_kafeelname`, `stinfo_kafeeladdress`, `stinfo_kafeelidno`, `stinfo_kafeeljob`) VALUES
(1, 'basim', 'mohammed', 'salih', 'ali', '', '', 'ذكر', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, 'mohammed', 'abas', 'ali', 'jasim', '', '', 'ذكر', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, '23', '', '', '', '', '', 'ذكر', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 'aaaa', '', '', '', '', '', 'ذكر', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(5, 'aaaa', '', '', '', '', '', 'Ø°ÙƒØ±', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6, 'aaaaaaaa', 'aaaaaaaa', 'salih', '', '', '', '', 'ذكر', '', '', '', '', '', '', '', '', '', '', '', ''),
(7, 'ÙˆØ§Ø­Ø¯', 'Ø«Ø§Ù†ÙŠ', 'Ø«Ø§Ù„Ø«', 'Ø§Ù„Ø¹Ø±Ø§Ù‚ÙŠ', 'ØªØ¨Ø§', 'Ù…Ø§', 'Ø°ÙƒØ±', 'Ø§Ù„Ø§Ù†Ø¨Ø§Ø±', 'omØ§Ù„Ù„@uofallujah.edu.iq', '8888', '', '55', '334', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `students_study`
--

DROP TABLE IF EXISTS `students_study`;
CREATE TABLE IF NOT EXISTS `students_study` (
  `stdy_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'الرقم التسلسلي',
  `stdy_idno` int(11) NOT NULL COMMENT 'رقم الطالب',
  `stdy_sceschool` varchar(255) NOT NULL COMMENT 'اسم الثانوية',
  `stdy_examno` varchar(100) NOT NULL COMMENT 'الرقم الامتحاني',
  `stdy_branch` int(11) NOT NULL COMMENT 'ادبي-علمي-تجاري-صناعي-زززالخ',
  `stdy_accchannel` int(11) NOT NULL COMMENT 'قناة القبول',
  `stdy_attemp` int(11) NOT NULL DEFAULT '1' COMMENT 'الدور الذي نجح فيه',
  `stdy_notes` longtext NOT NULL COMMENT 'ملاحظات',
  `stdy_sum` double NOT NULL DEFAULT '0' COMMENT 'مجموع الدرجات',
  `stdy_avg` double NOT NULL COMMENT 'معدل الطالب',
  PRIMARY KEY (`stdy_id`),
  KEY `fk1` (`stdy_idno`),
  KEY `fk22` (`stdy_accchannel`),
  KEY `fk23` (`stdy_branch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='معلومات الطالب الدراسية';

-- --------------------------------------------------------

--
-- Table structure for table `st_degree`
--

DROP TABLE IF EXISTS `st_degree`;
CREATE TABLE IF NOT EXISTS `st_degree` (
  `deg_id` int(11) NOT NULL AUTO_INCREMENT,
  `deg_nameobject` varchar(255) NOT NULL COMMENT 'المادة الدراسية',
  `deg_degree` double NOT NULL,
  `deg_idno` int(11) DEFAULT NULL,
  PRIMARY KEY (`deg_id`),
  KEY `fk5` (`deg_idno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول درجات الطالب في الثانوية';

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `user_name` varchar(255) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_phone` varchar(10) NOT NULL,
  `user_desc` varchar(255) NOT NULL,
  `user_notes` text NOT NULL,
  `user_fullname` varchar(255) NOT NULL,
  `user_job` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_pass` (`user_pass`,`user_type`),
  UNIQUE KEY `user_name` (`user_name`),
  KEY `fk36` (`user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users_type`
--

DROP TABLE IF EXISTS `users_type`;
CREATE TABLE IF NOT EXISTS `users_type` (
  `ust_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `ust_type` varchar(255) NOT NULL,
  `ust_colid` int(11) NOT NULL,
  `ust_desc` varchar(255) NOT NULL,
  `ust_notes` text NOT NULL,
  PRIMARY KEY (`ust_id`),
  KEY `fk35` (`ust_colid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `years`
--

DROP TABLE IF EXISTS `years`;
CREATE TABLE IF NOT EXISTS `years` (
  `yr_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي',
  `yr_year` varchar(10) NOT NULL COMMENT 'السنة الدراسيسة',
  `yr_notes` text NOT NULL COMMENT 'ملاحظات',
  PRIMARY KEY (`yr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `deparments`
--
ALTER TABLE `deparments`
  ADD CONSTRAINT `fk10` FOREIGN KEY (`dep_colid`) REFERENCES `college` (`col_id`);

--
-- Constraints for table `dep_students`
--
ALTER TABLE `dep_students`
  ADD CONSTRAINT `fk11` FOREIGN KEY (`dstd_stdid`) REFERENCES `students_info` (`stinfo_id`),
  ADD CONSTRAINT `fk20` FOREIGN KEY (`dstd_depid`) REFERENCES `deparments` (`dep_id`),
  ADD CONSTRAINT `fk21` FOREIGN KEY (`dstd_yearid`) REFERENCES `years` (`yr_id`);

--
-- Constraints for table `exceptions`
--
ALTER TABLE `exceptions`
  ADD CONSTRAINT `fk29` FOREIGN KEY (`exc_chid`) REFERENCES `channels` (`ch_id`),
  ADD CONSTRAINT `fk30` FOREIGN KEY (`exc_rulid`) REFERENCES `rules` (`rul_id`);

--
-- Constraints for table `favarate_objects`
--
ALTER TABLE `favarate_objects`
  ADD CONSTRAINT `fk27` FOREIGN KEY (`fav_ruleid`) REFERENCES `rules` (`rul_id`);

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `fk40` FOREIGN KEY (`perm_usrid`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `fk41` FOREIGN KEY (`perm_interface`) REFERENCES `interfaces` (`infc_id`);

--
-- Constraints for table `rules`
--
ALTER TABLE `rules`
  ADD CONSTRAINT `fk24` FOREIGN KEY (`rul_depid`) REFERENCES `deparments` (`dep_id`),
  ADD CONSTRAINT `fk25` FOREIGN KEY (`rul_branchid`) REFERENCES `branches` (`br_id`),
  ADD CONSTRAINT `fk26` FOREIGN KEY (`rul_channelid`) REFERENCES `channels` (`ch_id`),
  ADD CONSTRAINT `fk99` FOREIGN KEY (`rul_yrid`) REFERENCES `years` (`yr_id`);

--
-- Constraints for table `settings`
--
ALTER TABLE `settings`
  ADD CONSTRAINT `fk31` FOREIGN KEY (`set_chid`) REFERENCES `channels` (`ch_id`),
  ADD CONSTRAINT `fk33` FOREIGN KEY (`set_yrid`) REFERENCES `years` (`yr_id`);

--
-- Constraints for table `std_files`
--
ALTER TABLE `std_files`
  ADD CONSTRAINT `fk7` FOREIGN KEY (`f_stdid`) REFERENCES `students_info` (`stinfo_id`);

--
-- Constraints for table `students_study`
--
ALTER TABLE `students_study`
  ADD CONSTRAINT `fk1` FOREIGN KEY (`stdy_idno`) REFERENCES `students_info` (`stinfo_id`),
  ADD CONSTRAINT `fk22` FOREIGN KEY (`stdy_accchannel`) REFERENCES `channels` (`ch_id`),
  ADD CONSTRAINT `fk23` FOREIGN KEY (`stdy_branch`) REFERENCES `branches` (`br_id`),
  ADD CONSTRAINT `for1` FOREIGN KEY (`stdy_idno`) REFERENCES `students_info` (`stinfo_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `st_degree`
--
ALTER TABLE `st_degree`
  ADD CONSTRAINT `fk5` FOREIGN KEY (`deg_idno`) REFERENCES `students_info` (`stinfo_id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk36` FOREIGN KEY (`user_type`) REFERENCES `users_type` (`ust_id`);

--
-- Constraints for table `users_type`
--
ALTER TABLE `users_type`
  ADD CONSTRAINT `fk35` FOREIGN KEY (`ust_colid`) REFERENCES `college` (`col_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
