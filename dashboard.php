<?php
require 'includes/kernel.php';

middleware_admin_university();

include('includes/header.php');
include('includes/sidebar.php');
?>

	<div class="statistics row px-2">
		<div class="col-12 col-sm-6 col-md-4 statistics-item">
			<div class="item-details">
				<div>Admins</div>
				<div>123</div>
				<div class="icon-container">
					Hello
				</div>
				<div class="border"></div>
				<div class="item-footer">
					<a href="users.html">View users</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 statistics-item">
			<div class="item-details">
				<div>Admins</div>
				<div>123</div>
				<div class="icon-container">
					Hello
				</div>
				<div class="border"></div>
				<div class="item-footer">
					<a href="users.html">View users</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 statistics-item">
			<div class="item-details">
				<div>Admins</div>
				<div>123</div>
				<div class="icon-container">
					Hello
				</div>
				<div class="border"></div>
				<div class="item-footer">
					<a href="users.html">View users</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 statistics-item">
			<div class="item-details">
				<div>Admins</div>
				<div>123</div>
				<div class="icon-container">
					Hello
				</div>
				<div class="border"></div>
				<div class="item-footer">
					<a href="users.html">View users</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 statistics-item">
			<div class="item-details">
				<div>Admins</div>
				<div>123</div>
				<div class="icon-container">
					Hello
				</div>
				<div class="border"></div>
				<div class="item-footer">
					<a href="users.html">View users</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 statistics-item">
			<div class="item-details">
				<div>Admins</div>
				<div>123</div>
				<div class="icon-container">
					Hello
				</div>
				<div class="border"></div>
				<div class="item-footer">
					<a href="users.html">View users</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 statistics-item">
			<div class="item-details">
				<div>Admins</div>
				<div>123</div>
				<div class="icon-container">
					Hello
				</div>
				<div class="border"></div>
				<div class="item-footer">
					<a href="users.html">View users</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 statistics-item">
			<div class="item-details">
				<div>Admins</div>
				<div>123</div>
				<div class="icon-container">
					Hello
				</div>
				<div class="border"></div>
				<div class="item-footer">
					<a href="users.html">View users</a>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-6 col-md-4 statistics-item">
			<div class="item-details">
				<div>Admins</div>
				<div>123</div>
				<div class="icon-container">
					Hello
				</div>
				<div class="border"></div>
				<div class="item-footer">
					<a href="users.html">View users</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row chart-items pt-2">
		<div class="col-6 chart-item">
			<div class="chart-content p-3">
				<canvas id="myChart" width="400"></canvas>
			</div>
		</div>
	</div>

<?php
include('includes/footer.php');
?>