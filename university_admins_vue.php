<?php
include('includes/kernel.php');

middleware_admin_university();

include('includes/header.php');
include('includes/sidebar.php');
?>

<div class="card-container" id="university-admins">
	<div class="row">
		<div class="col-12 text-muted">
			<span class="fa fa-user"></span>
			<span class="lead ml-2">
				University Admins
			</span>
			<a href="university_admin_create.php" class="btn btn-success btn-sm float-right">
				<span class="mr-1">Add Admin</span>
				<span class="fa fa-plus"></span>
			</a>
		</div>
    <div class="col-12">
      <form class="row" id="search-form">
        <div class="col-12 col-md-6 mt-4">
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <span class="fa fa-search"></span>
              </div>
            </div>
            <input type="text" class="form-control" id="search-table" v-model="search" @input="fetchUsers" placeholder="Search" name="q">
          </div>
        </div>
        <div class="col-12 col-md-6 mt-4">
          <select v-model="amount" class="form-control" name="amount" id="select-amount" @input="fetchUsers()">
            <option :value="item" v-for="item in amounts">{{ item }}</option>
          </select>
        </div>
      </form>
    </div>

    <div class="col-12">
      <?php echo show_alerts(); ?>
    </div>

    <div class="col-12 mt-3">
    	<div class="table-responsive">
    		<table class="table table-hover">
    			<thead>
    				<tr>
    					<th>#</th>
    					<th>Name</th>
    					<th>Email</th>
    					<th>Phone</th>
    					<th>Job</th>
    					<th>Type</th>
    					<th>Description</th>
    					<th>Note</th>
              <th>Status</th>
              <th>Actions</th>
    				</tr>
    			</thead>
    			<tbody>
    				<tr v-for="(user, index) in users">
    					<td>{{ index + 1 }}</td>
    					<td>{{ user.user_name }}</td>
    					<td>{{ user.user_email }}</td>
              <td>{{ user.user_phone }}</td>
              <td>{{ user.user_job }}</td>
              <td>{{ user.user_type }}</td>
              <td>{{ user.user_desc }}</td>
              <td>{{ user.user_notes }}</td>
              <td>{{ user.user_status }}</td>
              <td>
                <a href="university_admin_edit.php?user_id=">
                  <span class="fa fa-edit"></span>
                </a>
                <span data-toggle="modal" data-target="#exampleModalCenter" class="text-danger">
                  <span class="fa fa-trash"></span>
                </span>
              </td>
    				</tr>
    			</tbody>
    		</table>
    	</div>
    </div>

	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Delete User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to delete this user!
      </div>
      <div class="modal-footer">
        <div class="d-flex justify-content-center w-100">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <form method="POST" action="university_admin_delete_process.php">
            <input type="" name="user_id" hidden id="deleted-user-id">
            <button type="submit" class="btn btn-danger ml-3">Confirm</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var app = new Vue({
    el: '#university-admins',
    data() {
      return {
        search: '',
        amount: 25,
        users: [],
        amounts: [1, 10, 25, 50, 100]
      }
    },
    methods: {
      fetchUsers() {
        setTimeout(() => {
          axios.get('university_admins_vue_json.php', {
            params: {
              amount: this.amount,
              q: this.search
            }
          })
          .then((res) => {
            this.users = res.data
          })
        }, 0)
      }
    },
    created() {
      this.fetchUsers()
    }
  })
</script>

<?php
include('includes/footer.php');
?>