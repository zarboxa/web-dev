<?php
include('includes/kernel.php');

middleware_admin_university();

include('includes/header.php');
include('includes/sidebar.php');

$page = 1;
$q = '';
$amounts = [1, 10, 25, 50, 100];

if (isset($_GET['q']) && !empty($_GET['q']) && is_string($_GET['q'])) {
  $q = $_GET['q'];
}

$result = get_result("SELECT COUNT(*) FROM users");

$total = $result['COUNT(*)'];
$amount = 10;

if (isset($_GET['amount']) && !empty($_GET['amount']) && is_numeric($_GET['amount']) && in_array($_GET['amount'], $amounts)) {
  $amount = $_GET['amount'];
}

$pageCount = ceil($total / $amount);

if (isset($_GET['page']) && !empty($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] <= $pageCount) {
  $page = $_GET['page'];
}

$offset = ($page - 1) * $amount;

$sql = "SELECT * FROM users";

if (!empty($q)) {
  $sql .= " WHERE user_name LIKE '%{$q}%' OR user_email LIKE '%{$q}'";
}

$sql .= " LIMIT {$amount} OFFSET {$offset}";

$users = get_results($sql);
?>

<div class="card-container">
	<div class="row">
		<div class="col-12 text-muted">
			<span class="fa fa-user"></span>
			<span class="lead ml-2">
				University Admins
			</span>
			<a href="university_admin_create.php" class="btn btn-success btn-sm float-right">
				<span class="mr-1">Add Admin</span>
				<span class="fa fa-plus"></span>
			</a>
		</div>
    <div class="col-12">
      <form class="row" id="search-form">
        <div class="col-12 col-md-6 mt-4">
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <span class="fa fa-search"></span>
              </div>
            </div>
            <input type="text" class="form-control" id="search-table" placeholder="Search" name="q" value="<?php echo $q; ?>">
          </div>
        </div>
        <div class="col-12 col-md-6 mt-4">
          <select class="form-control" name="amount" id="select-amount">
            <?php
            foreach ($amounts as $value) {
              echo "<option value='{$value}'" . ($value == $amount ? 'selected' : '') . ">{$value}</option>";
            }
            ?>
          </select>
        </div>
      </form>
    </div>
    <?php
    if ($pageCount > 1) {
    ?>
    <div class="col-12 mt-3">
			<ul class="pagination">
			  <!-- <li class="page-item">
			    <a class="page-link" href="#" aria-label="Previous">
			      <span aria-hidden="true">&laquo;</span>
			      <span class="sr-only">Previous</span>
			    </a>
			  </li> -->
        <?php
        for ($i=1; $i <= $pageCount; $i++) {
        ?>
        <li class="page-item <?php echo $i == $page ? 'active' : ''; ?>">
          <a class="page-link" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a>
        </li>
        <?php } ?>
			  <!-- <li class="page-item">
			    <a class="page-link" href="#" aria-label="Next">
			      <span aria-hidden="true">&raquo;</span>
			      <span class="sr-only">Next</span>
			    </a>
			  </li> -->
			</ul>
    </div>
    <?php } ?>

    <div class="col-12">
      <?php echo show_alerts(); ?>
    </div>

    <div class="col-12 mt-3">
    	<div class="table-responsive">
    		<table class="table table-hover">
    			<thead>
    				<tr>
    					<th>#</th>
    					<th>Name</th>
    					<th>Email</th>
    					<th>Phone</th>
    					<th>Job</th>
    					<th>Type</th>
    					<th>Description</th>
    					<th>Note</th>
              <th>Status</th>
              <th>Actions</th>
    				</tr>
    			</thead>
    			<tbody>
    				<?php
    				foreach ($users as $index => $user) {
    				?>
    				<tr>
    					<td><?php echo $index + 1; ?></td>
    					<td><?php echo $user['user_name']; ?></td>
    					<td><?php echo $user['user_email']; ?></td>
    					<td><?php echo $user['user_phone']; ?></td>
    					<td><?php echo $user['user_job']; ?></td>
    					<td><?php echo $user['user_type']; ?></td>
    					<td><?php echo $user['user_desc']; ?></td>
    					<td><?php echo $user['user_notes']; ?></td>
              <td><?php echo $user['user_status']; ?></td>
              <td>
                <a href="university_admin_edit.php?user_id=<?php echo $user['user_id']; ?>">
                  <span class="fa fa-edit"></span>
                </a>
                <span onclick="setDeletedUserId(<?php echo $user['user_id']; ?>)" data-toggle="modal" data-target="#exampleModalCenter" class="text-danger">
                  <span class="fa fa-trash"></span>
                </span>
              </td>
    				</tr>
	    			<?php } ?>
    			</tbody>
    		</table>
    	</div>
    </div>

	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Delete User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to delete this user!
      </div>
      <div class="modal-footer">
        <div class="d-flex justify-content-center w-100">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <form method="POST" action="university_admin_delete_process.php">
            <input type="" name="user_id" hidden id="deleted-user-id">
            <button type="submit" class="btn btn-danger ml-3">Confirm</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var el = document.getElementById('select-amount')
  var form = document.getElementById('search-form')
  el.oninput = function() {
    form.submit()
    console.log('select is changed!!!')
  }

  var items = document.querySelectorAll('.pagination .page-link')
  Array.from(items).forEach(function(item) {
    item.onclick = function(e) {
      e.preventDefault()

      var input = document.createElement('input')
      input.name = 'page'
      input.value = this.innerText
      input.hidden = 'hidden'

      form.appendChild(input)
      form.submit()
      console.log('on click page-link', e)
    }
  })

  function setDeletedUserId(id) {
    var input = document.getElementById('deleted-user-id')
    input.value = id
  }
</script>

<?php
include('includes/footer.php');
?>