<?php
include(__DIR__ . '/includes/kernel.php');

middleware_admin_university();

include('includes/header.php');
include('includes/sidebar.php');

$userTypes = get_results("SELECT * FROM user_types");

?>

<div class="card-container">
	<div class="row">
		<div class="col-12 col-md-8 m-auto">
			<form class="row" method="POST" action="university_admin_create_process.php">

				<div class="col-12">
					<?php echo show_alerts(); ?>
				</div>
				<div class="col-12 form-group">
					<label for="name">Name</label>
					<input id="name" type="text" class="form-control" name="user_name" placeholder="Name" value="<?php echo old_data('user_name'); ?>">
					<span class="help-text" id="name-errors"></span>
				</div>
				<div class="col-12 form-group">
					<label for="email">Email</label>
					<input id="email" type="email" class="form-control" name="user_email" placeholder="Email" value="<?php echo old_data('user_email'); ?>">
				</div>
				<div class="col-12 col-md-6 form-group">
					<label for="phone">Phone</label>
					<input id="phone" type="tel" class="form-control" name="user_phone" placeholder="Phone" value="<?php echo old_data('user_phone'); ?>">
				</div>
				<div class="col-12 col-md-6 form-group">
					<label for="job">Job</label>
					<input id="job" type="text" class="form-control" name="user_job" placeholder="Job" value="<?php echo old_data('user_job'); ?>">
				</div>
				<div class="col-12 col-md-6 form-group">
					<label for="type">Type</label>
					<select id="type" name="user_type" class="form-control">
						<?php
						foreach ($userTypes as $userType) {
							echo "<option value='{$userType['ust_id']}'>$userType[ust_type]</option>";
						}
						?>
						<option></option>
					</select>
				</div>
				<div class="col-12 col-md-6 form-group">
					<label for="description">Description</label>
					<input id="description" type="text" class="form-control" name="user_desc" placeholder="Description" value="<?php echo old_data('user_desc'); ?>">
				</div>
				<div class="col-12 form-group">
					<label for="note">Note</label>
					<textarea id="note" name="user_note" class="form-control" placeholder="Note ...">
						<?php echo old_data('user_note'); ?>
					</textarea>
				</div>
				<div class="col-12 form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	var el = document.getElementById('name')
	el.oninput = function() {
		var errors = []
		if (typeof this.value === 'string') {
			if (this.value.length < 2) {
				errors.push('Character should be more than 2')
			} else if (this.value.length > 100) {
				errors.push('Character should be less than 100')
			}
		} else {
			errors.push('This field must a string')
		}

		// var errorEl = document.getElementById('name-errors')
		// if (errors.length > 0) {
		// 	this.parentNode.classList.add('has-error')
		// 	errorEl.innerText = errors.join(', ')
		// } else {
		// 	errorEl.innerText = ''
		// 	this.parentNode.classList.remove('has-error')
		// }


		// console.log(this.value)
	}
</script>

<?php
include('includes/footer.php');
?>