-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2020 at 06:08 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ac_sys`
--

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `br_id` int(11) NOT NULL COMMENT 'الرقم التسلسلي',
  `br_name` varchar(255) NOT NULL COMMENT 'اسم الفرع'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول فروع او تخصص الثانوي ادبي- علمي - الخ';

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

CREATE TABLE `channels` (
  `ch_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `ch_name` varchar(255) NOT NULL COMMENT 'اسم قناة القبول'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول قنوات القبول';

-- --------------------------------------------------------

--
-- Table structure for table `colleges`
--

CREATE TABLE `colleges` (
  `col_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `col_name` varchar(100) NOT NULL COMMENT 'اسم الكلية',
  `col_max_std` int(11) NOT NULL COMMENT 'الحد الاعلى للطلبة',
  `col_desc` varchar(255) NOT NULL COMMENT 'وصف عام',
  `col_notes` text NOT NULL COMMENT 'ملاحظات'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `deparments`
--

CREATE TABLE `deparments` (
  `dep_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `dep_name` varchar(50) NOT NULL COMMENT 'اسم القسم',
  `dep_max_std` int(11) NOT NULL COMMENT 'الحد الاعلى من الطلاب',
  `dep_minsum` int(11) NOT NULL COMMENT 'الحد الادى من المجموع',
  `dep_minavg` double NOT NULL COMMENT 'الحد الادنى من المعدلات',
  `dep_desc` varchar(250) NOT NULL COMMENT 'وصف عام',
  `dep_notes` text NOT NULL COMMENT 'ملاحظات',
  `dep_colid` int(11) NOT NULL COMMENT 'الكلية'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول الاقسام';

-- --------------------------------------------------------

--
-- Table structure for table `department_students`
--

CREATE TABLE `department_students` (
  `dstd_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `dstd_stdid` int(11) NOT NULL COMMENT 'الطالب',
  `dstd_depid` int(11) NOT NULL COMMENT 'الكلية والقسم',
  `dstd_yearid` int(11) NOT NULL COMMENT 'السنة الدراسية',
  `dstd_desc` varchar(255) NOT NULL COMMENT 'وصف عام',
  `dstd_notes` text NOT NULL COMMENT 'ملاحظات'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول لربط الطلاب بالاقسام';

-- --------------------------------------------------------

--
-- Table structure for table `exceptions`
--

CREATE TABLE `exceptions` (
  `exc_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `exc_name` varchar(255) NOT NULL,
  `exc_chid` int(11) NOT NULL,
  `exc_numbr` int(11) NOT NULL,
  `exc_avg` float NOT NULL,
  `exc_rate` float NOT NULL,
  `exc_desc` varchar(255) NOT NULL,
  `exc_notes` text NOT NULL,
  `exc_rulid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `favourite_subjects`
--

CREATE TABLE `favourite_subjects` (
  `fav_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `fav_name` varchar(255) NOT NULL COMMENT 'اسم المادة',
  `fav_mindeg` float NOT NULL COMMENT 'الحد الادنى من درجة المادة ',
  `fav_ruleid` int(11) NOT NULL COMMENT 'رقم قانون التقديم من جدول القوانين',
  `fav_desc` varchar(255) NOT NULL COMMENT 'وصف عام',
  `fav_notes` text NOT NULL COMMENT 'ملاحظات'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول دروس المفاضلة';

-- --------------------------------------------------------

--
-- Table structure for table `interfaces`
--

CREATE TABLE `interfaces` (
  `infc_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `infc_name` varchar(255) NOT NULL COMMENT 'اسم الواجهة',
  `infc_desc` varchar(255) NOT NULL COMMENT 'وصف عام',
  `infc_notes` text NOT NULL COMMENT 'ملاحظات'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول الواجهات';

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `perm_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `perm_usrid` int(11) NOT NULL,
  `perm_interface` int(11) NOT NULL,
  `perm_full` tinyint(1) NOT NULL,
  `perm_exec` tinyint(1) NOT NULL,
  `perm__insert` tinyint(1) NOT NULL,
  `perm_update` tinyint(1) NOT NULL,
  `perm_delete` tinyint(1) NOT NULL,
  `perm_grantto` tinyint(1) NOT NULL,
  `perm_desc` varchar(255) NOT NULL,
  `perm_notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `rules`
--

CREATE TABLE `rules` (
  `rul_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `rul_depid` int(11) NOT NULL COMMENT 'القسم والكلية والسنة الدراسية',
  `rul_sex` varchar(10) NOT NULL COMMENT 'ذكر او انثى او اخرى',
  `rul_branchid` int(11) NOT NULL COMMENT 'رقم الفرع في الثانوية',
  `rul_rate` double NOT NULL COMMENT 'نسبة حسب التحديدات',
  `rul_number` int(11) NOT NULL COMMENT 'العدد حسب التحديدات',
  `rul_avgfrom` float NOT NULL COMMENT 'المعدل من',
  `rul_avgto` float NOT NULL COMMENT 'المعدل الى ',
  `rul_sumfrom` int(11) NOT NULL COMMENT 'المجموع من',
  `rul_sumto` int(11) NOT NULL COMMENT 'المجموع الى',
  `rul_channelid` int(11) NOT NULL COMMENT 'رقم قناة القبول',
  `rul_desc` varchar(255) NOT NULL COMMENT 'وصف عام',
  `rul_notes` text NOT NULL COMMENT 'ملاحظات',
  `rul_yrid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول قوانين التقديم على الاقسام';

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `set_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `set_adddeg_attempt1` float NOT NULL COMMENT 'اضافة درجة للدور الاول',
  `set_chid` int(11) NOT NULL COMMENT ' قناة القبول',
  `set_chid_degree` float NOT NULL COMMENT 'اضافة درجة حسب قناة القبول',
  `set_end_date` date NOT NULL COMMENT 'تاريخ نهاية التقديم',
  `set_desc` varchar(255) NOT NULL,
  `set_notes` text NOT NULL,
  `set_yrid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول الاعدادات العامة';

-- --------------------------------------------------------

--
-- Table structure for table `student_degrees`
--

CREATE TABLE `student_degrees` (
  `deg_id` int(11) NOT NULL,
  `deg_nameobject` varchar(255) NOT NULL COMMENT 'المادة الدراسية',
  `deg_degree` double NOT NULL,
  `deg_idno` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول درجات الطالب في الثانوية';

-- --------------------------------------------------------

--
-- Table structure for table `student_files`
--

CREATE TABLE `student_files` (
  `f_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `f_filename` varchar(100) NOT NULL COMMENT 'اسم الملف',
  `f_type` varchar(50) NOT NULL COMMENT 'نوع الملف',
  `f_size` int(11) NOT NULL COMMENT 'حجم الملف',
  `f_content` mediumblob NOT NULL COMMENT 'محتوى',
  `f_stdid` int(11) NOT NULL COMMENT 'رقم الطالب',
  `f_notes` text NOT NULL COMMENT 'ملاحظات',
  `f_desc` varchar(100) NOT NULL COMMENT 'اسم المستمسك'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `student_info`
--

CREATE TABLE `student_info` (
  `stinfo_id` int(11) NOT NULL COMMENT 'الرقم التسلسلي',
  `stinfo_name1` varchar(25) NOT NULL COMMENT 'الاسم الاول',
  `stinfo_name2` varchar(25) NOT NULL COMMENT 'اسم الاب',
  `stinfo_name3` varchar(25) NOT NULL COMMENT 'اسم الجد',
  `stinfo_name4` varchar(25) NOT NULL COMMENT 'اسم الجد الرابع',
  `stinfo_surname` varchar(25) NOT NULL COMMENT 'اللقب',
  `stinfo_mothername` varchar(100) NOT NULL COMMENT 'اسم الام الثلاثي',
  `stinfo_sex` varchar(50) NOT NULL DEFAULT 'ذكر' COMMENT 'الجنس',
  `stinfo_address` varchar(250) NOT NULL COMMENT 'العنوان',
  `stinfo_email` varchar(150) NOT NULL COMMENT 'البريد الالكتروني',
  `stinfo_phone` varchar(150) NOT NULL COMMENT 'رقم الموبايل',
  `stinfo_notes` longtext NOT NULL COMMENT 'الملاحظات',
  `stinfo_idcard` varchar(250) NOT NULL COMMENT 'رقم البطاقة',
  `stinfo_idno` varchar(250) NOT NULL COMMENT 'رقم الجنسية',
  `stinfo_certid` varchar(250) NOT NULL COMMENT 'رقم شهادة الجنسية',
  `stinfo_foodcardno` varchar(250) NOT NULL COMMENT 'ؤقم بطاقة العذائية',
  `stinfo_addresscard` varchar(250) NOT NULL COMMENT 'بطاقة السكن',
  `stinfo_kafeelname` varchar(255) NOT NULL COMMENT 'اسم الكفيل',
  `stinfo_kafeeladdress` varchar(255) NOT NULL COMMENT 'عنوان الكفيل',
  `stinfo_kafeelidno` varchar(255) NOT NULL COMMENT 'رقم هاتف الكفيل',
  `stinfo_kafeeljob` varchar(255) NOT NULL COMMENT 'جهة انتساب الكفيل'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='جدول معلومات الطلاب عامة ';

--
-- Dumping data for table `student_info`
--

INSERT INTO `student_info` (`stinfo_id`, `stinfo_name1`, `stinfo_name2`, `stinfo_name3`, `stinfo_name4`, `stinfo_surname`, `stinfo_mothername`, `stinfo_sex`, `stinfo_address`, `stinfo_email`, `stinfo_phone`, `stinfo_notes`, `stinfo_idcard`, `stinfo_idno`, `stinfo_certid`, `stinfo_foodcardno`, `stinfo_addresscard`, `stinfo_kafeelname`, `stinfo_kafeeladdress`, `stinfo_kafeelidno`, `stinfo_kafeeljob`) VALUES
(1, 'basim', 'mohammed', 'salih', '', '', '', 'ذكر', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, 'alaa', 'mohammed', 'salih', '', '', '', 'ذكر', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, 'jumaa', 'mohammed', 'salih', '', '', '', 'ذكر', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `student_studies`
--

CREATE TABLE `student_studies` (
  `stdy_id` bigint(20) NOT NULL COMMENT 'الرقم التسلسلي',
  `stdy_idno` int(11) NOT NULL COMMENT 'رقم الطالب',
  `stdy_sceschool` varchar(255) NOT NULL COMMENT 'اسم الثانوية',
  `stdy_examno` varchar(100) NOT NULL COMMENT 'الرقم الامتحاني',
  `stdy_branch` int(11) NOT NULL COMMENT 'ادبي-علمي-تجاري-صناعي-زززالخ',
  `stdy_accchannel` int(11) NOT NULL COMMENT 'قناة القبول',
  `stdy_attemp` int(11) NOT NULL DEFAULT 1 COMMENT 'الدور الذي نجح فيه',
  `stdy_notes` longtext NOT NULL COMMENT 'ملاحظات',
  `stdy_sum` double NOT NULL DEFAULT 0 COMMENT 'مجموع الدرجات',
  `stdy_avg` double NOT NULL COMMENT 'معدل الطالب'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='معلومات الطالب الدراسية';

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `user_name` varchar(255) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_phone` varchar(10) NOT NULL,
  `user_desc` varchar(255) NOT NULL,
  `user_notes` text NOT NULL,
  `user_fullname` varchar(255) NOT NULL,
  `user_job` varchar(255) NOT NULL,
  `col_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `ust_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `ust_type` varchar(255) NOT NULL,
  `ust_desc` varchar(255) NOT NULL,
  `ust_notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `years`
--

CREATE TABLE `years` (
  `yr_id` int(11) NOT NULL COMMENT 'رقم التسلسلي',
  `yr_year` varchar(10) NOT NULL COMMENT 'السنة الدراسيسة',
  `yr_notes` text NOT NULL COMMENT 'ملاحظات'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`br_id`);

--
-- Indexes for table `channels`
--
ALTER TABLE `channels`
  ADD PRIMARY KEY (`ch_id`);

--
-- Indexes for table `colleges`
--
ALTER TABLE `colleges`
  ADD PRIMARY KEY (`col_id`);

--
-- Indexes for table `deparments`
--
ALTER TABLE `deparments`
  ADD PRIMARY KEY (`dep_id`),
  ADD KEY `fk10` (`dep_colid`);

--
-- Indexes for table `department_students`
--
ALTER TABLE `department_students`
  ADD PRIMARY KEY (`dstd_id`),
  ADD KEY `fk11` (`dstd_stdid`),
  ADD KEY `fk20` (`dstd_depid`),
  ADD KEY `fk21` (`dstd_yearid`);

--
-- Indexes for table `exceptions`
--
ALTER TABLE `exceptions`
  ADD PRIMARY KEY (`exc_id`),
  ADD KEY `fk29` (`exc_chid`),
  ADD KEY `fk30` (`exc_rulid`);

--
-- Indexes for table `favourite_subjects`
--
ALTER TABLE `favourite_subjects`
  ADD PRIMARY KEY (`fav_id`),
  ADD KEY `fk27` (`fav_ruleid`);

--
-- Indexes for table `interfaces`
--
ALTER TABLE `interfaces`
  ADD PRIMARY KEY (`infc_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`perm_id`),
  ADD KEY `fk40` (`perm_usrid`),
  ADD KEY `fk41` (`perm_interface`);

--
-- Indexes for table `rules`
--
ALTER TABLE `rules`
  ADD PRIMARY KEY (`rul_id`),
  ADD KEY `fk24` (`rul_depid`),
  ADD KEY `fk25` (`rul_branchid`),
  ADD KEY `fk26` (`rul_channelid`),
  ADD KEY `fk99` (`rul_yrid`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`set_id`),
  ADD KEY `fk31` (`set_chid`),
  ADD KEY `fk33` (`set_yrid`);

--
-- Indexes for table `student_degrees`
--
ALTER TABLE `student_degrees`
  ADD PRIMARY KEY (`deg_id`),
  ADD KEY `fk5` (`deg_idno`);

--
-- Indexes for table `student_files`
--
ALTER TABLE `student_files`
  ADD PRIMARY KEY (`f_id`),
  ADD KEY `fk7` (`f_stdid`);

--
-- Indexes for table `student_info`
--
ALTER TABLE `student_info`
  ADD PRIMARY KEY (`stinfo_id`);

--
-- Indexes for table `student_studies`
--
ALTER TABLE `student_studies`
  ADD PRIMARY KEY (`stdy_id`),
  ADD KEY `fk1` (`stdy_idno`),
  ADD KEY `fk22` (`stdy_accchannel`),
  ADD KEY `fk23` (`stdy_branch`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_pass` (`user_pass`,`user_type`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD KEY `fk36` (`user_type`),
  ADD KEY `ffk` (`col_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`ust_id`);

--
-- Indexes for table `years`
--
ALTER TABLE `years`
  ADD PRIMARY KEY (`yr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `br_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'الرقم التسلسلي';

--
-- AUTO_INCREMENT for table `channels`
--
ALTER TABLE `channels`
  MODIFY `ch_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `colleges`
--
ALTER TABLE `colleges`
  MODIFY `col_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `deparments`
--
ALTER TABLE `deparments`
  MODIFY `dep_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `department_students`
--
ALTER TABLE `department_students`
  MODIFY `dstd_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `exceptions`
--
ALTER TABLE `exceptions`
  MODIFY `exc_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `favourite_subjects`
--
ALTER TABLE `favourite_subjects`
  MODIFY `fav_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `interfaces`
--
ALTER TABLE `interfaces`
  MODIFY `infc_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `perm_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `rules`
--
ALTER TABLE `rules`
  MODIFY `rul_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `set_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `student_degrees`
--
ALTER TABLE `student_degrees`
  MODIFY `deg_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_files`
--
ALTER TABLE `student_files`
  MODIFY `f_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `student_info`
--
ALTER TABLE `student_info`
  MODIFY `stinfo_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'الرقم التسلسلي', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `student_studies`
--
ALTER TABLE `student_studies`
  MODIFY `stdy_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'الرقم التسلسلي';

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `ust_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- AUTO_INCREMENT for table `years`
--
ALTER TABLE `years`
  MODIFY `yr_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'رقم التسلسلي';

--
-- Constraints for dumped tables
--

--
-- Constraints for table `deparments`
--
ALTER TABLE `deparments`
  ADD CONSTRAINT `fk10` FOREIGN KEY (`dep_colid`) REFERENCES `colleges` (`col_id`);

--
-- Constraints for table `department_students`
--
ALTER TABLE `department_students`
  ADD CONSTRAINT `fk11` FOREIGN KEY (`dstd_stdid`) REFERENCES `student_info` (`stinfo_id`),
  ADD CONSTRAINT `fk20` FOREIGN KEY (`dstd_depid`) REFERENCES `deparments` (`dep_id`),
  ADD CONSTRAINT `fk21` FOREIGN KEY (`dstd_yearid`) REFERENCES `years` (`yr_id`);

--
-- Constraints for table `exceptions`
--
ALTER TABLE `exceptions`
  ADD CONSTRAINT `fk29` FOREIGN KEY (`exc_chid`) REFERENCES `channels` (`ch_id`),
  ADD CONSTRAINT `fk30` FOREIGN KEY (`exc_rulid`) REFERENCES `rules` (`rul_id`);

--
-- Constraints for table `favourite_subjects`
--
ALTER TABLE `favourite_subjects`
  ADD CONSTRAINT `fk27` FOREIGN KEY (`fav_ruleid`) REFERENCES `rules` (`rul_id`);

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `fk40` FOREIGN KEY (`perm_usrid`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `fk41` FOREIGN KEY (`perm_interface`) REFERENCES `interfaces` (`infc_id`);

--
-- Constraints for table `rules`
--
ALTER TABLE `rules`
  ADD CONSTRAINT `fk24` FOREIGN KEY (`rul_depid`) REFERENCES `deparments` (`dep_id`),
  ADD CONSTRAINT `fk25` FOREIGN KEY (`rul_branchid`) REFERENCES `branches` (`br_id`),
  ADD CONSTRAINT `fk26` FOREIGN KEY (`rul_channelid`) REFERENCES `channels` (`ch_id`),
  ADD CONSTRAINT `fk99` FOREIGN KEY (`rul_yrid`) REFERENCES `years` (`yr_id`);

--
-- Constraints for table `settings`
--
ALTER TABLE `settings`
  ADD CONSTRAINT `fk31` FOREIGN KEY (`set_chid`) REFERENCES `channels` (`ch_id`),
  ADD CONSTRAINT `fk33` FOREIGN KEY (`set_yrid`) REFERENCES `years` (`yr_id`);

--
-- Constraints for table `student_degrees`
--
ALTER TABLE `student_degrees`
  ADD CONSTRAINT `fk5` FOREIGN KEY (`deg_idno`) REFERENCES `student_info` (`stinfo_id`);

--
-- Constraints for table `student_files`
--
ALTER TABLE `student_files`
  ADD CONSTRAINT `fk7` FOREIGN KEY (`f_stdid`) REFERENCES `student_info` (`stinfo_id`);

--
-- Constraints for table `student_studies`
--
ALTER TABLE `student_studies`
  ADD CONSTRAINT `fk1` FOREIGN KEY (`stdy_idno`) REFERENCES `student_info` (`stinfo_id`),
  ADD CONSTRAINT `fk22` FOREIGN KEY (`stdy_accchannel`) REFERENCES `channels` (`ch_id`),
  ADD CONSTRAINT `fk23` FOREIGN KEY (`stdy_branch`) REFERENCES `branches` (`br_id`),
  ADD CONSTRAINT `for1` FOREIGN KEY (`stdy_idno`) REFERENCES `student_info` (`stinfo_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `ffk` FOREIGN KEY (`col_id`) REFERENCES `colleges` (`col_id`),
  ADD CONSTRAINT `fk36` FOREIGN KEY (`user_type`) REFERENCES `user_types` (`ust_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
