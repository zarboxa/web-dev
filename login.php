<?php
require 'includes/kernel.php';

middleware_guest();

?>
<!DOCTYPE html>
<html dir="rtl">
<head>
	<title>Dashboard</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css?kj">
	<link rel="stylesheet" type="text/css" href="css/dashboard.css?md">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>

<div class="row align-items-center justify-content-center" style="min-height: 100vh">
	<div class="col-6">
		<div class="card-container">
			<div class="text-center h3">Login</div>
			<?php echo show_alerts(); ?>
			<form method="POST" action="login_process.php">
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" name="email" placeholder="Email" class="form-control" value="<?php echo old_data('email'); ?>">
				</div>
				<div class="form-group mb-4">
					<label for="password">Password</label>
					<input type="password" name="password" placeholder="Password" class="form-control">
				</div>
				<div class="form-group">
					<button class="btn btn-primary w-100">Login</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
include('includes/footer.php');
?>